import sys

from fpdf import FPDF  # fpdf2: https://pypi.org/project/fpdf2/  fpdf2 version 2.0.5 by Oliver PLATHEY, ported by Max
# This version of FPDF has a bug.  multi_cell does not move the cursor.  Use regular fpdf to work around.

import logging
from logging import handlers
import sys
import datetime
# Developed with Python 3.7.6

# Setup Logging
log = logging.getLogger('')
log.setLevel(logging.DEBUG)
format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# Standard output logging
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(format)
log.addHandler(ch)

# Output logs to file
fh = handlers.RotatingFileHandler('output/debug.log', maxBytes=(1048576*5), backupCount=7)
fh.setFormatter(format)
log.addHandler(fh)


class ManagePDF:
    """
    Dispatch individual and group billings to respective class in order to generate PDFs.
    This is necessary as the header in FPDF cannot be called from within the same instance, and needs to be called from
    a unique individual instance.
    """
    def __init__(self):
        pass


class GenerateExercisePDF(FPDF):
    """
    Generate a PDF file for devices that are billed individually.
    """
    def __init__(self, exercises, layout, **kwargs):
        """
        Initialization of object.
        :param name: currently placeholder for future name of exercise.
        """

        self.exercises = exercises  # list of dictionaries with information for each exercise.
        self.image_height = 35  # default insert image height
        self.image_width = 35  # default insert image width
        self.multi_cell_w = 90  # default multi_cell width
        self.multi_cell_height = 7  # default multi_cell height
        self.header_h = 15
        self.header_bottom_h = 0

        self.exercises_added = 0

        # Dynamically update save file path.
        self.path = 'output/' + 'exercise_list' + '.pdf'
        if 'path' in kwargs:
            self.path = kwargs['path']

        # Layout Settings
        self.layout = layout  # the layout to use for the PDF output.
        self.layout_2_r_offset = 110

        # Override FPDF defaults
        super(FPDF, self).__init__()
        self.figcount = 1

        # Initialize FPDF document
        FPDF.__init__(self, 'P', 'mm', 'letter')

        # Write PDF files for each of the individuals to be billed.
        self.create_document()
        for entry in self.exercises:  # add text for each exercise entry.
            self.add_text(entry)
        self.alias_nb_pages()  # fill in number of pages before writing PDF.
        self.output_pdf(self.path)
        self.document = None

    def header(self):
        # Position at 1.5 cm from top
        self.set_y(self.header_h)
        self.set_font('Arial', '', 15)

        self.cell(40, 10, 'Exercise List', 1, 0, 'C')
        today = datetime.date.today().strftime("%B %d, %Y")
        self.cell(0, 10, today, 0, 1, 'R')

        # Line break
        self.ln(5)

        self.header_bottom_h = self.get_y()

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

    def create_document(self):
        """
        Create PDF document and page layout.
        :return:
        """

        self.set_margins(15, 25, 15)
        self.add_page()
        self.set_font('Arial', size=12)

    def add_text(self, entry):
        """
        Layout and write text to cells.
        :return: None
        """

        layout = entry['layout']

        def layout_1():
            logging.log(logging.DEBUG, 'layout_1()')
            lo_1_line_h = 7  # layout 1 line height

            start_positon = self.get_x(), self.get_y()
            logging.log(logging.DEBUG, 'Before image insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

            current_y = self.get_y()

            # tracks the lowest position of items in the layout in order to determine whether to insert a page break.
            lowest_y = self.get_y()

            def track_page_break(height):
                nonlocal lowest_y
                if lowest_y < (current_y + height):
                    lowest_y = lowest_y + height
                    logging.log(logging.DEBUG, 'lowest_y assignment: ' + str(lowest_y))

                logging.log(logging.DEBUG, 'track_page_break(height): ' + str(lowest_y) + ' self.page_break_trigger: ' +
                            str(self.page_break_trigger))

                # Return False if lowest_y is less than the page_break_trigger, indicating no page break is necessary.
                if lowest_y < self.page_break_trigger:
                    return False
                else:
                    return True

            # Check that inserting an image does not trigger a page break.
            track_page_break(self.image_height)
            # Check that the multi_cell wont trigger a page break
            multi_cell_lines = self.multi_cell(self.multi_cell_w, self.multi_cell_height,
                            entry['instructions'], 1, 1, 'L', split_only=True)
            track_page_break(len(multi_cell_lines) * self.multi_cell_height)
            # Check that the activity prescription text block won't trigger a page break
            page_break = track_page_break(5 * lo_1_line_h)
            logging.log(logging.DEBUG, 'projected layout height ending y: ' + str(lowest_y))

            # if the layout block would extend into the next page, trigger a page break before inserting.
            if page_break is True:
                self.add_page()
                current_y = self.get_y()  # Set the current_y to the new position on the next page.

            logging.log(logging.DEBUG, 'Image height: ' + str(self.image_height))
            print('self.exercises: ', self.exercises)
            try:
                self.image(entry['image'], w=self.image_width, h=self.image_height)
            except FileNotFoundError as e:
                print(e)
            except KeyError as e:
                print(e)
            logging.log(logging.DEBUG, 'After image insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

            # Align cursor to the right of the image.
            self.set_xy((self.get_x() + self.image_width + 0), current_y)


            print('self.get_x(): ', self.get_x())

            # multi_cell block
            logging.log(logging.DEBUG, 'Before multi_cell: ' + str(self.get_y()))

            logging.log(logging.DEBUG, 'multi_cell lines: ' + str(len(multi_cell_lines)))
            logging.log(logging.DEBUG, 'multi_cell units taken: ' + str(len(multi_cell_lines) * self.multi_cell_height))

            self.multi_cell(self.multi_cell_w, self.multi_cell_height,
                            entry['instructions'], 1, 1, 'L')
            logging.log(logging.DEBUG, 'After multi_cell: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

            def set_cursor_new_line():
                current_y = self.get_y()
                self.set_xy((self.get_x() + self.image_width + 0 + self.multi_cell_w), current_y)

            # Exercise parameters
            # Align cursor to the right of the multi_cell.

            # Works with FPDF original.  For PDF2, the multicell is placing the cursor to the right of the multicell,
            # not at the bottom like FDPF does.  Uncomment to use FPDF original.
            # self.set_xy((self.get_x() + self.image_width + 0 + self.multi_cell_w), current_y)

            logging.log(logging.DEBUG, 'Before activity prescription block x: ' +
                        str(self.get_x()) + ' y: ' + str(self.get_y()))
            self.cell(40, lo_1_line_h, 'Activity: ' + str(entry['name']), 0, 1, 'L')
            logging.log(logging.DEBUG,
                        'After Activity Insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))
            set_cursor_new_line()
            logging.log(logging.DEBUG,
                        'After Activity set_cursor_new_line(): x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))
            logging.log(logging.DEBUG,
                        'Before Repetitions Insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))
            self.cell(40, lo_1_line_h, 'Repetitions: ' + str(entry['repetitions']), 0, 1, 'L')
            print('entry[repetitions]): ', entry['repetitions'])
            logging.log(logging.DEBUG,
                        'After Repetitions Insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))
            set_cursor_new_line()
            logging.log(logging.DEBUG,
                        'After Repetitions set_cursor_new_line(): x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))
            self.cell(40, lo_1_line_h, 'Hold ' + str(entry['hold duration']) + ' ' + str(entry['hold units']), 0, 1, 'L')
            set_cursor_new_line()
            self.cell(40, lo_1_line_h, 'Complete ' + str(entry['sets']) + ' ' + 'set(s)', 0, 1, 'L')
            set_cursor_new_line()
            self.cell(40, lo_1_line_h, 'Perform  ' + str(entry['times per period']) + ' times ' + 'per ' +
                      str(entry['period duration']), 0, 1, 'L')
            logging.log(logging.DEBUG, 'After activity prescription block: ' + str(self.get_y()))

            logging.log(logging.DEBUG, 'actual layout height ending y: ' + str(self.get_y()))

            self.ln()
            self.ln()

            self.exercises_added += 1

        def layout_2():
            horizontal_y_start = 0
            if self.exercises_added % 2 < 1:
                # Layout with multi_cell on bottom.
                start_position = self.get_x(), self.get_y()
                horizontal_y_start = self.get_y()
                logging.log(logging.DEBUG, 'Start position: Left Block: ' + str(start_position))

                logging.log(logging.DEBUG, 'layout on left')

                logging.log(logging.DEBUG, 'Before image insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

                current_y = self.get_y()
                try:
                    self.image('images/001.jpg', w=self.image_width, h=self.image_height)
                except FileNotFoundError as e:
                    print(e)
                logging.log(logging.DEBUG, 'After image insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

                # Align cursor to the right of the image.
                self.set_xy((self.get_x() + self.image_width + 0), current_y)

                print('self.get_x(): ', self.get_x())

                # Align cursor to the right of the multi_cell.
                #self.set_xy((self.get_x() + 0), current_y)

                def set_cursor_new_line():
                    # for layout 2.
                    current_y = self.get_y()
                    self.set_xy((self.get_x() + self.image_width + 0), current_y)

                # Exercise parameters

                self.cell(40, 7, 'Activity: ' + entry['name'], 0, 1, 'L')
                set_cursor_new_line()

                self.cell(40, 7, 'Repetitions: ' + entry['repetitions'], 0, 1, 'L')
                set_cursor_new_line()
                self.cell(40, 7, 'Hold ' + entry['hold duration'] + ' ' + entry['hold units'], 0, 1, 'L')
                set_cursor_new_line()
                self.cell(40, 7, 'Complete ' + entry['sets'] + ' ' + 'set(s)', 0, 1, 'L')
                set_cursor_new_line()
                self.cell(40, 7, 'Perform  ' + entry['times per period'] + ' times ' + 'per ' + entry['period duration'], 0, 1, 'L')

                self.ln()

                # multi_cell block
                logging.log(logging.DEBUG, 'Before multi_cell: ' + str(self.get_y()))
                self.multi_cell(self.multi_cell_w, self.multi_cell_height,
                                entry['instructions'],
                                1, 1, 'L')
                self.current_y = self.get_y()
                logging.log(logging.DEBUG, 'After multi_cell: ' + str(self.current_y))

                self.ln()
                self.ln()

                self.exercises_added += 1

            else:
                # Exercise block to the right
                start_position = self.get_x(), (horizontal_y_start + self.header_bottom_h)
                logging.log(logging.DEBUG, 'Start position: Right Block: ' + str(start_position))

                logging.log(logging.DEBUG, 'layout on right')
                self.set_xy(*start_position)

                start_position = self.get_x(), self.get_y()
                logging.log(logging.DEBUG, 'Before image insert: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

                self.set_x(self.layout_2_r_offset)

                current_y = self.get_y()
                try:
                    self.image('images/001.jpg', w=self.image_width, h=self.image_height)
                except FileNotFoundError as e:
                    print(e)
                logging.log(logging.DEBUG, 'After image insert Right: x: ' + str(self.get_x()) + ' y: ' + str(self.get_y()))

                # Align cursor to the right of the image.
                self.set_xy((self.image_width + self.layout_2_r_offset), current_y)

                print('self.get_x(): ', self.get_x())

                # Align cursor to the right of the multi_cell.
                # self.set_xy((self.get_x() + 0), current_y)

                def set_cursor_new_line():
                    # for layout 2.
                    current_y = self.get_y()
                    self.set_xy((self.image_width + self.layout_2_r_offset), current_y)

                # Exercise parameters

                self.cell(40, 7, 'Activity: ' + 'Supine Bridge TODO', 0, 1, 'L')
                set_cursor_new_line()

                self.cell(40, 7, 'Repetitions ' + '5 TODO', 0, 1, 'L')
                set_cursor_new_line()
                self.cell(40, 7, 'Hold ' + '1 second TODO', 0, 1, 'L')
                set_cursor_new_line()
                self.cell(40, 7, 'Complete ' + '1  TODO' + 'set(s)', 0, 1, 'L')
                set_cursor_new_line()
                self.cell(40, 7, 'Perform  ' + 'time ' + 'per ' + 'day TODO', 0, 1, 'L')

                self.ln()

                # Set cursor below right image
                current_y = self.get_y()
                self.set_xy((self.layout_2_r_offset), current_y)

                # multi_cell block
                logging.log(logging.DEBUG, 'Before multi_cell: ' + str(self.get_y()))
                self.multi_cell(self.multi_cell_w, self.multi_cell_height,
                                entry['instructions'],
                                1, 1, 'L')
                self.current_y = self.get_y()
                logging.log(logging.DEBUG, 'After multi_cell: ' + str(self.current_y))

                self.ln()
                self.ln()

                self.exercises_added += 1

        if self.layout is 1:
            layout_1()
        elif self.layout is 2:
            layout_2()

        # Test code to determine page break
        logging.log(logging.DEBUG, "current page_break_trigger: " + str(self.page_break_trigger))
        logging.log(logging.DEBUG, "current y: " + str(self.get_y()))

    def lowest_y(self):
        pass

    def output_pdf(self, path):
        """
        Output PDF for individual device/user.
        :return: None.
        """

        try:
            # self.output('output/' + 'exercise_list' + '.pdf')  # Windows format default path.
            self.output(path)  # Windows format.
        except OSError:
            log.exception("PDF output file is not writeable.")


if __name__ == '__main__':
    log.debug('Program start.')

    # Sample testing data.
    import Sample_Data
    data = Sample_Data.Data

    generate_pdf = GenerateExercisePDF(data.exercise, 1)
    log.debug('Program end.\n')
