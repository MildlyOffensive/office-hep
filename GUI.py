import tkinter as tk
from tkinter import ttk
import tkinter.messagebox
import tkinter.filedialog as fd
import re
import logging
import Generate_PDF as g_pdf
import os as os
import Sample_Data
import tkinter.simpledialog
import tkinter.scrolledtext
import win32print as wprt
import win32api as wapi


log = logging.getLogger('')
log.debug('GUI.py')


class MainApplication(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        # Variables
        self.megabytes_path = None
        self.minutes_path = None
        self.messages_path = None

        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        # root = Tk()
        root.title('Office Exercises')
        root.geometry('{}x{}'.format(880, 600))

        # Mouse wheel event handling for scrolling
        def mouse_wheel(event):
            """ Mouse wheel as scroll bar """
            direction = 0

            # Handles scrolling of the scrolled canvas.  Ignores scroll command if the canvas contents are smaller in
            # height than it's parent frame.
            if details_list.page_4_canvas.bbox("all")[3] > ctr_mid.bbox("all")[3]:

                # respond to Linux or Windows wheel event
                if event.num == 5 or event.delta == -120:
                    direction = 1
                if event.num == 4 or event.delta == 120:
                    direction = -1

                details_list.page_4_canvas.yview_scroll(direction, "units")
                log.debug('Scroll command received.  Scrolling.')
            # print(direction)
            else:
                log.debug('Scroll command received.  Scroll ignored.')

            # DEBUG:
            log.debug('details_list.page_4_canvas.bbox("all"): ' + str(details_list.page_4_canvas.bbox("all")))
            log.debug('ctr_mid.bbox("all"): ' + str(ctr_mid.bbox("all")))

        def onFrameConfigure(canvas):
            """Reset the scroll region to encompass the inner frame.  Cannot be applied to a notebook"""
            canvas.configure(scrollregion=canvas.bbox("all"))

        # Placer method for menubar.
        # TODO: Complete functions for menubar buttons
        def donothing():
            filewin = tk.Toplevel(root)
            button = tk.Button(filewin, text="Do nothing button")
            button.pack()

        # Add menubar to root window
        menubar = tk.Menu(root)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="New", command=donothing)
        filemenu.add_command(label="Open", command=donothing)
        filemenu.add_command(label="Save", command=donothing)
        filemenu.add_command(label="Save as...", command=donothing)
        filemenu.add_command(label="Close", command=donothing)

        filemenu.add_separator()

        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)
        editmenu = tk.Menu(menubar, tearoff=0)
        editmenu.add_command(label="Undo", command=donothing)

        editmenu.add_separator()

        editmenu.add_command(label="Cut", command=donothing)
        editmenu.add_command(label="Copy", command=donothing)
        editmenu.add_command(label="Paste", command=donothing)
        editmenu.add_command(label="Delete", command=donothing)
        editmenu.add_command(label="Select All", command=donothing)

        menubar.add_cascade(label="Edit", menu=editmenu)
        helpmenu = tk.Menu(menubar, tearoff=0)
        helpmenu.add_command(label="Help Index", command=donothing)
        helpmenu.add_command(label="About...", command=donothing)
        menubar.add_cascade(label="Help", menu=helpmenu)

        root.config(menu=menubar)
        # End Menubar section

        # create all of the main containers
        top_frame = tk.Frame(root, bg='gray90', width=200, height=50, pady=3)
        center = tk.Frame(root, bg='gray2', width=200, height=40, padx=3, pady=3)
        btm_frame = tk.Frame(root, bg='white', width=200, height=45, pady=3)
        # btm_frame2 = Frame(root, bg='lavender', width=450, height=60, pady=3)

        # layout all of the main containers
        root.grid_rowconfigure(1, weight=1)
        root.grid_columnconfigure(0, weight=1)

        top_frame.grid(row=0, sticky="ew")
        center.grid(row=1, sticky="nsew")
        btm_frame.grid(row=3, sticky="ew")
        # btm_frame2.grid(row=4, sticky="ew")


        # create the center widgets
        center.grid_rowconfigure(1, minsize=160, weight=2)
        center.grid_rowconfigure(0, minsize=0, weight=0)
        # center.grid_rowconfigure(1, minsize=5, weight=0)  # Spacer column
        # center.grid_rowconfigure(2, minsize=250, weight=1)
        center.grid_columnconfigure(0, minsize=160, weight=1)  # Spacer column
        center.grid_columnconfigure(1, minsize=0, weight=0)  # Spacer column

        """
        center.grid_columnconfigure(1, minsize=5, weight=0)  # Spacer column
        center.grid_columnconfigure(2, minsize=400, weight=0)
        center.grid_columnconfigure(3, minsize=5, weight=0)  # Spacer column
        center.grid_columnconfigure(4, minsize=400, weight=1)
        center.grid_columnconfigure(5, minsize=5, weight=0)  # Spacer column
        # center.grid_columnconfigure(6, minsize=320, weight=1)
        """

        # Define ctr_mid frame
        ctr_mid = tk.Frame(center, bg='white', width=200, height=120)
        ctr_mid.grid(row=1, column=0, sticky="nsew", columnspan=10, rowspan=1)

        # Contains the widgets above ctr_mid
        ctr_top = tk.Frame(center, bg='white', width=200, height=20)
        ctr_top.grid(row=0, column=0, sticky="nsew", columnspan=10, rowspan=1)

        # Configure the columns for ctr_top
        ctr_top.grid_columnconfigure(0, minsize=0, weight=0)
        ctr_top.grid_columnconfigure(1, minsize=400, weight=0)

        # Listbox to contain the exercise groups
        class ListboxObjects:
            """
            Scrollable Listbox with selector.
            """
            def __init__(self, parent_window, frame_effect):
                """

                :param parent_window: The parent window that the ListBox is inserted into.
                :param frame_effect: The frame we want our selection to effect.  Use the integer of the frame in
                ScrolledCanvas.frames[]
                """
                self.window = parent_window

                def onselect(evt):
                    w = evt.widget
                    #index = int(w.curselection()[0])
                    #value = w.get(index)
                    #log.debug('You selected item %d: "%s"' % (index, value))
                    #log.debug('Corresponding exercise group: %s' % (exercise_groups.exercise_group_layout[index]))

                    # Fill checkmark for group items in scrolled window
                    if frame_effect == 0:
                        self.select_action_exercises()

                    if frame_effect == 1:
                        self.group_action_exercises()

                self.frame = tk.Frame(self.window)
                # self.frame.grid(row=0, column=1, sticky="nsew", columnspan=1, rowspan=1)
                self.scrollbar = tk.Scrollbar(self.frame, orient=tk.VERTICAL)
                self.listbox = tk.Listbox(self.frame, yscrollcommand=self.scrollbar.set, bg='white', height=6,
                                          exportselection=False, borderwidth=0, highlightthickness=0)
                #self.listbox.bind("<<ListboxSelect>>", onselect)  # Use when single click selecting the listbox
                self.listbox.bind("<Double-Button-1>", onselect)  # Use when double click selecting the listbox
                self.scrollbar.config(command=self.listbox.yview)
                self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
                self.listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

            def select_action_exercises(self):
                """
                Marks the checkboxes for the exercises in the selected exercise group.
                :param index: the index of the item selected from the listbox
                :param value: the dictionary key for list of integers indicating the exercise IDs we want to select from
                 the listbox.
                :return:
                """

                # the index of the item selected from the listbox
                index = int(self.listbox.curselection()[0])

                # the dictionary key for list of integers indicating the exercise IDs we want to select from
                # the listbox.
                value = self.listbox.get(index)
                log.log(logging.DEBUG, 'Exercise Groups Listbox: cursor selection: index: ' + str(index))
                for item in details_list.frames[0][2]:
                    item.restore_row()  # restore all rows to screen in case removed.
                    item.select_button.deselect()  # clear the checkbuttons before selection.
                    for entry in exercise_groups.exercise_group_layout[index][value]:
                        # print('entry 1', entry)
                        # print('entry[id]: ', entry['id'])
                        # print('item.id', item.id)

                        print('item.id: ', item.id, 'entry: ', entry)
                        # Check the corresponding checkbutton for each exercise in the selected exercise group.
                        if entry['id'] == item.id:
                            print('item.id: ', item.id, 'entry: ', entry)
                            item.select_button.select()
                            # Update parameters with stored group parameters
                            item.title_label_tag.set(entry['name'])
                            item.repetitions_entry_var.set(entry['repetitions'])
                            item.hold_entry_var.set(entry['hold duration'])
                            item.hold_units_var.set(entry['hold units'])
                            item.sets_entry_var.set(entry['sets'])
                            item.freq_num_entry_var.set(entry['times per period'])
                            item.freq_int_option_var.set(entry['period duration'])
                            print('Selected: exercise_groups.exercise_group_layout[index][value]',
                                  exercise_groups.exercise_group_layout[index][value])
                details_list.frames[0][0].lift(aboveThis=None)  # bring frame to top of ScrolledCanvas

            def group_action_exercises(self):
                print('group_action_exercises')
                if details_list.frames[1][0]:
                    print('details_list.frames[1][0]', details_list.frames[1][0])
                    hide_frame_rows(1)

                index = int(self.listbox.curselection()[0])

                # the dictionary key for list of integers indicating the exercise IDs we want to select from
                # the listbox.
                value = self.listbox.get(index)
                log.log(logging.DEBUG, 'Exercise Groups Listbox: cursor selection: index: ' + str(index) +
                        '. Value: ' + str(value))
                for counter, entry in enumerate(details_list.frames):
                    hide_frame_rows(counter)

                print('data.exercise_types: ', data.exercise_types)
                key = self.listbox.get(index)
                for entry in data.exercise_types:
                    if key in entry:
                        # Get/Iterate over the key we want.
                        print('details_list.frames[0]', details_list.frames[0])
                        print('self.listbox.get(index)', self.listbox.get(index))  # the listbox index contents
                        #print('entry[index]: ', str(entry[index]))
                        print('entry: ', entry)

                        for counter, listing in enumerate(entry[key]):  # for each listing that has the selected string
                            print('entry ', entry)
                            print('listing[id] ', listing['id'])
                            print('entry[key][0][id]', entry[key][0]['id'])
                            for exercise in data.exercise:  # check over and add any exercises that match ID
                                if listing['id'] == exercise['id']:
                                    print('listing', listing)
                                    print('data.exercise', data.exercise)
                                    print('counter', counter)
                                    print('details_list.frames[0][2]', details_list.frames[0][2])
                                    print('entry: ', str(entry))
                                    print('exercise:', str(exercise))
                                    for SCR in details_list.frames[0][2]:
                                        if SCR.id == exercise['id']:
                                            # Restore the row to the screen if the id matches the id in the selected
                                            # exercise group
                                            SCR.restore_row()
                                            # Restore the default displayed exercise values.
                                            # SCR.update_row_values(SCR, exercise)

                # Code if we used a separate frame.
                # details_list.frames[1][0].lift(aboveThis=None)  # bring frame to top of ScrolledCanvas
                # TODO: Insert associated exercises.

        # exercise_group_sel = tk.Listbox(ctr_top, height=5, width=2)
        exercise_group_sel = ListboxObjects(ctr_top, 0)
        exercise_group_sel.frame.grid(row=0, column=1, sticky="nsew", columnspan=1, rowspan=1, padx=(4,0))

        # Selector commands frame.  Frame to contain the buttons for the exercise group selector listbox.
        sel_comm_frame = tk.Frame(ctr_top, bg='white', width=200)
        sel_comm_frame.grid(row=0, column=0, sticky="nsew", columnspan=1, rowspan=1)

        # Buttons
        group_selector_sel = tk.Button(sel_comm_frame, text='Select', width=6,
                                       command=exercise_group_sel.select_action_exercises)
        group_selector_sel.grid(row=0, column=0)

        def prompt_new(listbox_objects, type):
            ex_group_name = tk.simpledialog.askstring('New Exercise Group', 'Name')  # ask for exercise group name

            if ex_group_name is not None:
                if type is 'sets':
                    # Open a prompt for a new name of an exercise group, and add the group to the selector.
                    selected_exercises = []
                    for item in details_list.frames[0][2]:
                        if item.select_button_val.get() == True:
                            # Retrieves the currently selected values from the window to use.
                            current_items = item.retrieve_current_values(item)  # Temp hold dictionary entries

                            # Parameters from selected entries.
                            # selected_exercises['row'] = item.row
                            """
                            # This code segment retrieves the values from the dictionary instead of the window
                            current_items['id'] = item.id
                            current_items['name'] = item.title
                            current_items['repetitions'] = item.repetitions
                            current_items['hold duration'] = item.hold
                            current_items['hold units'] = item.hold_units_var.get()
                            current_items['sets'] = item.sets
                            current_items['times per period'] = item.freq_num
                            current_items['period duration'] = item.freq_int_option_var.get()
                            current_items['instructions'] = None
                            current_items['layout'] = 1
                            """

                            selected_exercises.append(current_items)  # insert each dictionary into temp list structure.

                    log.log(logging.DEBUG, 'prompt_new(): Selected New Exercise IDs: ' + str(selected_exercises))

                    # add the new exercise group
                    exercise_groups.exercise_group_layout.append({ex_group_name: selected_exercises})
                    listbox_objects.listbox.insert(tk.END, str(ex_group_name))
                    print('exercise_groups.exercise_group_layout: ', exercise_groups.exercise_group_layout)

                # For exercise regions or groups
                if type is 'groups':
                    selected_exercises = {ex_group_name: []}  # data structure of exercise types
                    for item in details_list.frames[0][2]:
                        if item.select_button_val.get() == True:
                            # Retrieves the currently selected values from the window to use.
                            selected_exercises[ex_group_name].append({'id': item.id})  # Temp hold dictionary entries

                    listbox_objects.listbox.insert(tk.END, str(ex_group_name))  # insert new entry at bottom of listbox
                    data.exercise_types.append(selected_exercises)  # insert the new exercise group into data

        group_selector_add = tk.Button(sel_comm_frame, text='New', width=6,
                                       command=lambda: prompt_new(exercise_group_sel, 'sets'))
        group_selector_add.grid(row=1, column=0)

        group_selector_edit = tk.Button(sel_comm_frame, text='Edit', width=6)
        group_selector_edit.grid(row=2, column=0)

        group_selector_delete = tk.Button(sel_comm_frame, text='Delete', width=6)
        group_selector_delete.grid(row=3, column=0)

        # Exercise Selector listbox manage.
        # For exercise group selector horizontally across upper mid portion of window.
        class ExerciseGroups:
            def __init__(self, listbox_s, exercise_groups, start=0):
                self.listbox = listbox_s
                self.exercise_groups = exercise_groups  # list with single dictionary in each index.
                # Dictionary contains a list of integers indicating the exercise ID for each exercise group.
                self.start = start

                self.exercise_group_layout = []  # List with the positions of each exercise group entry in the exercise
                # selector listbox.

                self.insert_exercise_groups(self.exercise_groups, self.start)

            def insert_exercise_groups(self, exercise_groups, start=0):
                for index, exercise_group in enumerate(exercise_groups, start=start):
                    self.exercise_group_layout.append(exercise_group)
                    for entry in exercise_group:
                        self.listbox.listbox.insert(index, str(entry))
                log.log(logging.DEBUG, 'exercise_groups: self.exercise_group_layout: ' +
                        str(self.exercise_group_layout))

        """
        # Testing code.  Add test data to the exercise selector listbox.
        for item in range(0,30):
            exercise_group_sel.listbox.insert(item, item)
        """

        """
        # paste center grid frames
        ctr_left = tk.Frame(center, bg='blue', width=250, height=190)
        ctr_mid = tk.Frame(center, bg='yellow', width=250, height=190)
        ctr_mid_right = tk.Frame(center, bg='teal', width=250, height=190)
        ctr_right = tk.Frame(center, bg='green', width=250, height=190)

        ctr_left.grid(row=0, column=0, sticky="ns", rowspan=3)
        ctr_mid.grid(row=0, column=2, sticky="nsew")
        ctr_mid_right.grid(row=0, column=4, sticky="nsew")
        ctr_right.grid(row=0, column=6, sticky="nsew", rowspan=3)
        """

        # Configure center grid sub frames
        # ctr_mid_right.grid_columnconfigure(1, weight=0, minsize=200)

        """
        # paste bottom grid frames
        btm_left = Frame(btm_frame, bg='green', width=250, height=190, padx=3, pady=3)
        btm_mid = Frame(btm_frame, bg='green', width=250, height=190, padx=3, pady=3)
        btm_mid_right = Frame(btm_frame, bg='teal', width=250, height=190, padx=3, pady=3)
        btm_right = Frame(btm_frame, bg='green', width=250, height=190, padx=3, pady=3)
        """

        """
        # paste bottom grid frames
        # btm_left = Frame(center, bg='green', width=250, height=190, padx=3, pady=3)
        btm_mid = tk.Frame(center, bg='green', width=250, height=190)
        btm_mid_right = tk.Frame(center, bg='teal', width=250, height=190)
        # btm_right = Frame(center, bg='green', width=250, height=190, padx=3, pady=3)

        # btm_left.grid(row=1, column=0, sticky="ns") #
        btm_mid.grid(row=2, column=2, sticky="nsew")
        btm_mid_right.grid(row=2, column=4, sticky="nsew")
        # btm_right.grid(row=1, column=3, sticky="nsew")
        """

        # ctr_mid.grid_columnconfigure(0, weight=1)

        # Gather exercise output values
        def gather_output_values():
            selected_rows = []  # Contains rows that have been selected.
            for row in details_list.frames[0][2]:  # TODO: need to address what happens if other frames are selected.
                if row.select_button_val.get() is True:
                    selected_rows.append(row)  # add the integer tracking the rows that have been selected.
            log.debug('Selected Rows: ' + str(selected_rows))

            output_data = []  # Contains dictionaries of row data to output to PDF.
            for row in selected_rows:
                exercise = {
                    'id': row.id,
                    'name': row.title,
                    'repetitions': row.repetitions,
                    'hold duration': row.hold,
                    'hold units': row.hold_units_var.get(),
                    'sets': row.sets,
                    'times per period': row.freq_num,
                    'period duration': row.freq_int,
                    'instructions': row.instructions,
                    'layout': 1,
                    'image': row.image
                }
                output_data.append(exercise)
            print("output data: ", output_data)
            log.debug('Output Data list of selected dictonaries: ' + str(output_data))
            return output_data

        def exercise_selected():
            """
            checks to see if an exercise is selected.
            :return: a list with Booleans in each position.  False for unselected checkbox.  True for selected checkbox.
            """
            selected_exercises = []
            for entry in ScrolledCanvas.frames[0][2]:
                selected_exercises.append(entry.select_button_val.get())
            return selected_exercises

        # File output
        def file_save():
            selected_exercises = exercise_selected()

            # exit without processing further if no exercises are selected.
            if True not in selected_exercises:
                log.log(logging.DEBUG, 'Output button pressed.  No exercises selected.')
                return False

            f = fd.asksaveasfilename(confirmoverwrite=True, defaultextension=".pdf")
            if f is (None or ''):  # asksaveasfile return `None` if dialog closed with "cancel".
                print('Canceled')
                return
            else:
                print('File Output:', f)
                output_values = gather_output_values()
                generate_pdf = g_pdf.GenerateExercisePDF(output_values, 1, path=f)  # TODO: fix output.
                return f

        output_button = tk.Button(top_frame, text="Output", command=file_save)
        output_button.grid(row=0, column=0, padx=5, pady=1, sticky="w")

        def pdf_print():
            """
            Print document command for print button.
            :return:
            """

            # Check if exercises are selected.
            selected_exercises = exercise_selected()

            # exit without processing further if no exercises are selected.
            if True not in selected_exercises:
                log.log(logging.DEBUG, 'Print button pressed.  No exercises selected.')
                return False

            output_values = gather_output_values()

            def installed_printer():
                """
                returns a tuple with installed printers.
                :return: Tuple of tuples containing printer information. Inner tuples:
                [0]: Identical device ID.
                [1]: Full printer name with comments and/or address.
                [2]: Printer name.  Can be sent to win32api.ShellExecute()
                [3]: Blank entry.
                """
                printers = wprt.EnumPrinters(2)
                print('Default Printer: ', wprt.GetDefaultPrinter())

                """
                # Used for debugging the list of printers.
                for entry_index, printer in enumerate(printers):
                    for printer_subitems, entry in enumerate(printer):
                        log.log(logging.DEBUG, 'Printer entry index: ' + str(entry_index) + ' Printer Subitem: ' +
                                str(printer_subitems) + ' Entry: ' + str(entry))
                """

                return printers

            def locprinter():
                """
                Brings up a Toplevel() window that allows us to locate a printer and send a print job.
                :return: None
                """
                pt = tk.Toplevel()
                pt.geometry("350x250")
                pt.title("choose printer")
                printer_label = tk.Label(pt, text="select Printer").pack()
                pr_combo = ttk.Combobox(pt, width=55)
                pr_combo['values'] = printers
                pr_combo.pack()
                refresh_btn = ttk.Button(pt, text="refresh", command=installed_printer).pack()

                print('Default Printer: ', wprt.GetDefaultPrinter())
                print('Default Printer Type: ', str(type(wprt.GetDefaultPrinter())))

                def print_cmd():
                    selected_printer = pr_combo.get()
                    selected_printer_id = selected_printer
                    print('selected_printer: ', selected_printer)
                    print('selected_printer_id: ', selected_printer_id)
                    filename = r"output\exercise_list.pdf"

                    if selected_printer == (None or ''):
                        print('print cancelled')
                        print('selected_printer value: ', '|' + str(selected_printer) + '|')
                        print('selected_printer type: ', '|', type(selected_printer), '|')

                    else:
                        #os.startfile("output\exercise_list.pdf", "print")  # Use to print straight to default printer.

                        # Works with "printto".  'print' seems to always use the default printer.
                        wapi.ShellExecute(
                            0,
                            "printto",
                            #"print",
                            filename,
                            #
                            # If this is None, the default printer will
                            # be used anyway.
                            #
                            #'/d:"%s"' % selected_printer_id,
                            '"%s"' % selected_printer_id,
                            ".",
                            0
                        )

                        print('print triggered')
                        print('selected_printer value: ', '|' + str(selected_printer) + '|')
                        print('selected_printer type: ', '|', type(selected_printer), '|')

                print_btn = ttk.Button(pt, text="Print", command=print_cmd).pack()

            generate_pdf = g_pdf.GenerateExercisePDF(output_values, 1)  # TODO: fix output.

            printers = []
            printers_full = installed_printer()
            for printer in printers_full:
                printers.append(printer[2])
            #printers = installed_printer()
            print('Installed Printers: ', installed_printer())
            locprinter()

        print_button = tk.Button(top_frame, text="Print", command=pdf_print)
        print_button.grid(row=0, column=1, padx=5, pady=1, sticky="w")

        # User & Device List
        class dev_listbox_frame:
            def __init__(self, users_list):
                self.users_list = users_list

                log.debug('dev_listbox_frame: self.users_list: ' + str(self.users_list))

                def onselect(evt):
                    w = evt.widget
                    index = int(w.curselection()[0])
                    value = w.get(index)
                    log.debug('You selected item %d: "%s"' % (index, value))
                    for element in self.users_list:
                        if self.users_list[element][5] == int(index):
                            print(self.users_list[element][5])
                            insert_user_values(self.users_list[element][3])

                frame = tk.Frame(ctr_mid)
                scrollbar = tk.Scrollbar(frame, orient=tk.VERTICAL)
                data = tk.Listbox(frame, yscrollcommand=scrollbar.set, bg='white')
                data.bind("<<ListboxSelect>>", onselect)
                scrollbar.config(command=data.yview)
                scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
                data.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

                """
                for i in range(1000):
                    data.insert(tk.END, str(i))
                """

                for index, user in enumerate(self.users_list, start=0):
                    data.insert(tk.END, str(user))
                    self.users_list[user].append(index)

                frame.grid(row=0, column=0, rowspan=4,
                           columnspan=2, sticky=tk.N + tk.E + tk.S + tk.W)
                frame.columnconfigure(0, weight=1)

        # Resize contents of ctr_mid
        ctr_mid.grid_columnconfigure(0, weight=0)
        ctr_mid.grid_columnconfigure(1, weight=0)
        ctr_mid.grid_columnconfigure(2, weight=2)
        ctr_mid.grid_rowconfigure(0, weight=0)
        ctr_mid.grid_rowconfigure(1, weight=2)
        # ctr_mid.grid_rowconfigure(1, weight=1)

        class ScrolledCanvas:
            # Structure to contain the frames of a Scrolled Canvas.
            # Contains a nested list.  Inner list: [Frame, Frame_ID, Frame Rows of ScrolledCanvasRow]
            frames = [[None, None, []]]

            def append_frames_entry(self):
                """
                Appends a new list structure to frames list, so we don't have to keep track of the list layout.
                :return: length of ScrolledCanvas.frames after appending.
                """
                ScrolledCanvas.frames.append([None, None, []])
                return len(ScrolledCanvas.frames)

            def __init__(self):
                self.canvas_position = 0
                #self.rows = []  # the ScrolledCanvasRow objects contained
                #self.frames[0].bind("<Configure>", self.reset_scrollregion)  # Binds & Updates the Canvas window.

                self.bind_scrollbar_frame(0)

            def bind_scrollbar_frame(self, frame):
                """
                Binds & Updates the canvas scroll region dynamically.  Use to bind to a new frame.
                :param frame:
                :return:
                """
                self.frames[frame][0].bind("<Configure>", self.reset_scrollregion)  # Binds & Updates the Canvas window

            # Block 4
            # Paste canvas onto nb1_tab3 to contain widgets & scrollbar
            # TODO: make wider to contain scrollbar
            page_4_canvas = tk.Canvas(ctr_mid, width=100, height=80)
            #page_4_canvas.configure(scrollregion = page_4_canvas.bbox("all"))
            #print("bbox.all: ", page_4_canvas.bbox("all"))
            #page_4_canvas.pack(fill=tk.BOTH, expand=1)
            page_4_canvas.grid(row=0, column=2, sticky=tk.N + tk.E + tk.S + tk.W, columnspan=50, rowspan=50)

            # Paste vertical scrollbar next to canvas in Block 4
            vbar4 = tk.Scrollbar(page_4_canvas, orient=tk.VERTICAL)
            vbar4.pack(side=tk.RIGHT, fill=tk.Y, expand=0)
            vbar4.config(command=page_4_canvas.yview)

            # Insert frame into canvas to contain scrollable content.
            # frame4 = tk.Frame(page_4_canvas)
            frames[0][0] = tk.Frame(page_4_canvas)

            # Paste canvas next to scrollbar in Block 4
            page_4_canvas.config()
            page_4_canvas.config(yscrollcommand=vbar4.set)
            frames[0][1] = page_4_canvas.create_window((5, 5), window=frames[0][0], anchor='nw')
            # page_4_canvas.grid()

            # Mouse bindings for page_4_canvas
            def _bind_to_mousewheel(event):
                details_list.page_4_canvas.bind_all("<MouseWheel>", mouse_wheel)
                print('bind to page_4_canvas')

            def _unbind_from_mousewheel(event):
                details_list.page_4_canvas.unbind_all("<MouseWheel>")
                print('unbind from page_4_canvas')

            def reset_scrollregion(self, event):
                self.page_4_canvas.configure(scrollregion=self.page_4_canvas.bbox("all"))

            page_4_canvas.bind('<Enter>', _bind_to_mousewheel)
            page_4_canvas.bind('<Leave>', _unbind_from_mousewheel)

        details_list = ScrolledCanvas()

        class ScrolledCanvasRow:
            """
            def __init__(self, frame, row, title, right, repetitions=0, hold=0, sets=0, frequency_number=0,
                         frequency_interval=0):
            """

            @classmethod
            def restore_default_values(cls):
                """
                Restores all the rows of the ScrolledCanvas to the default values from Data.
                :return:
                """
                for exercise in data.exercise:  # check over and add any exercises that match ID
                    for SCR in details_list.frames[0][2]:
                        if SCR.id == exercise['id']:
                            # Restore the row to the screen if the id matches the id in the selected
                            # exercise group
                            SCR.restore_row()
                            SCR.select_button.deselect()
                            # Restore the default displayed exercise values.
                            SCR.update_row_values(SCR, exercise)

            def __init__(self, frame, row, parameters_dict):
                self.parameters_dict = parameters_dict

                self.row = row
                self.id = None
                self.title = None
                self.repetitions = None
                self.hold = None
                self.hold_units = None
                self.sets = None
                self.freq_num = None
                self.freq_int = None
                self.instructions = None
                self.image = None  # To contain path to image.

                if 'id' in parameters_dict:
                    self.id = parameters_dict['id']
                if 'name' in parameters_dict:
                    self.title = parameters_dict['name']
                if 'repetitions' in parameters_dict:
                    self.repetitions = parameters_dict['repetitions']
                if 'hold duration' in parameters_dict:
                    self.hold = parameters_dict['hold duration']
                if 'hold units' in parameters_dict:
                    self.hold_units = parameters_dict['hold units']
                if 'sets' in parameters_dict:
                    self.sets = parameters_dict['sets']
                if 'times per period' in parameters_dict:
                    self.freq_num = parameters_dict['times per period']
                if 'period duration' in parameters_dict:
                    self.freq_int = parameters_dict['period duration']
                if 'instructions' in parameters_dict:
                    self.instructions = parameters_dict['instructions']
                if 'image' in parameters_dict['image']:
                    self.image = parameters_dict['image']

                self.title_label_tag = tk.StringVar()
                self.value_label_tag_value = tk.StringVar()

                self.title_label_tag.set(self.title)
                self.value_label_tag_value.set(self.title)

                # Select Button
                self.select_button_val = tk.BooleanVar()

                def read_var():
                    print('Row', row, 'Checkbutton:', self.select_button_val.get())
                self.select_button = tk.Checkbutton(frame, text=None, var=self.select_button_val, command=read_var)
                self.select_button.grid(row=self.row, column=0, padx=0, sticky="w")

                # insert labels into frame4. Left side.
                self.label_tag = tk.Label(frame, textvariable=self.title_label_tag, justify=tk.LEFT,
                                          wraplength=100)
                self.label_tag.grid(row=self.row, column=10, padx=0, sticky="w")

                # insert labels for repetitions tag.
                self.repetitions_label = tk.Label(frame, text="Repetitions:", justify=tk.LEFT, wraplength=160)
                self.repetitions_label.grid(row=row, column=20, padx=0, sticky="w")

                # insert repetitions entry box.
                self.repetitions_entry_var = tk.StringVar()
                self.repetitions_entry_var.set(self.repetitions)
                self.repetitions_entry = tk.Entry(frame, textvariable=self.repetitions_entry_var,
                                                  justify=tk.LEFT, width=3)
                self.repetitions_entry.grid(row=row, column=30, padx=0, sticky="w")

                # insert labels for repetitions tag.
                self.hold_label = tk.Label(frame, text="hold:", justify=tk.LEFT, wraplength=160)
                self.hold_label.grid(row=row, column=40, padx=0, sticky="w")

                # insert repetitions entry box.
                self.hold_entry_var = tk.StringVar()
                self.hold_entry_var.set(self.hold)
                self.hold_entry = tk.Entry(frame, textvariable=self.hold_entry_var, justify=tk.LEFT, width=3)
                self.hold_entry.grid(row=row, column=50, padx=0, sticky="w")

                # insert Hold Units Combobox box.
                self.hold_units_var = tk.StringVar()
                self.hold_units_var.set(self.hold_units)
                def hold_units_selection_event(event):  # Log what the menu selection was.
                    logging.log(logging.DEBUG, 'Hold Units: ' + 'Title: ' + str(self.title) + '. Row: ' + str(self.row)
                                + ' Value: ' + str(self.hold_units.get()))
                self.hold_units = ttk.Combobox(frame, textvariable=self.hold_units_var, values=['seconds', 'minutes'])
                self.hold_units.grid(row=row, column=55, padx=0, sticky="w")
                self.hold_units.config(width=8)
                self.hold_units.bind("<<ComboboxSelected>>", hold_units_selection_event)

                # Unbind global combobox scrollwheel bindings, so comboboxes dont scroll with the main window.
                # Affects all comboboxes
                # Windows & OSX
                self.hold_units.unbind_class("TCombobox", "<MouseWheel>")

                # Linux and other *nix systems:
                self.hold_units.unbind_class("TCombobox", "<ButtonPress-4>")
                self.hold_units.unbind_class("TCombobox", "<ButtonPress-5>")

                # insert labels for sets tag.
                self.sets_label = tk.Label(frame, text="Complete:", justify=tk.LEFT, wraplength=160)
                self.sets_label.grid(row=row, column=60, padx=0, sticky="w")

                # insert sets entry box.
                self.sets_entry_var = tk.StringVar()
                self.sets_entry_var.set(self.sets)
                self.sets_entry = tk.Entry(frame, textvariable=self.sets_entry_var, justify=tk.LEFT, width=3)
                self.sets_entry.grid(row=row, column=70, padx=0, sticky="w")

                # insert labels for sets postfix tag.
                self.sets_label2 = tk.Label(frame, text="sets.", justify=tk.LEFT, wraplength=160)
                self.sets_label2.grid(row=row, column=75, padx=0, sticky="w")

                # insert labels for frequency number tag.
                self.freq_num_label = tk.Label(frame, text="Perform:", justify=tk.LEFT, wraplength=160)
                self.freq_num_label.grid(row=row, column=80, padx=0, sticky="w")

                # insert sets frequency number box.
                self.freq_num_entry_var = tk.StringVar()
                self.freq_num_entry_var.set(self.freq_num)
                self.freq_num_entry = tk.Entry(frame, textvariable=self.freq_num_entry_var,
                                               justify=tk.LEFT, width=3)
                self.freq_num_entry.grid(row=row, column=90, padx=0, sticky="w")

                # insert labels for frequency interval tag.
                self.freq_int_label = tk.Label(frame, text="time(s) per:",
                                               justify=tk.LEFT, wraplength=160)
                self.freq_int_label.grid(row=row, column=100, padx=0, sticky="w")

                # insert frequency interval Combobox.
                self.freq_int_option_var = tk.StringVar()
                self.freq_int_option_var.set(self.freq_int)
                self.freq_int_option = ttk.Combobox(frame, textvariable=self.freq_int_option_var,
                                                    values=['minute', 'hour', 'day'])

                # Log the value selected in the combobox.
                def cb_callback(event):
                    # pulling value straight from combobox, not variable.
                    logging.log(logging.DEBUG, 'Frequency Interval: ' + 'Title: ' + str(self.title) + '. Row: ' +
                                str(self.row) + ' Value: ' + str(self.freq_int_option.get()))
                self.freq_int_option.bind("<<ComboboxSelected>>", cb_callback)
                self.freq_int_option.config(width=7)
                self.freq_int_option.grid(row=row, column=115, padx=0, sticky="ew")

                # Edit button
                def edit_command():
                    print('Row', row, 'Edit button clicked.')

                    # Bring edit window up.

                    self.edit_window = tk.Toplevel(m_app)

                    # loads the exercise we want to edit into a new window for editing.
                    # edit_row = ScrolledCanvasRow(self.edit_window, 0, parameters_dict)
                    parent_row_values = self.retrieve_current_values(ScrolledCanvas.frames[0][2][row])
                    edit_row = ScrolledCanvasRow(self.edit_window, 0, parent_row_values)
                    edit_row.select_button.grid_remove()
                    edit_row.edit_button.grid_remove()

                    def update_exercise():
                        print('Update Exercise Clicked')

                        current_values = self.retrieve_current_values(edit_row)
                        # add the image location from the entry box img_location_entry
                        current_values['instructions'] = text_box.get("1.0", "end-1c")
                        current_values['image'] = img_location_entry_var.get()
                        # self.update_row_values(ScrolledCanvas.frames[0][2][row], current_values)
                        self.update_row_values(ScrolledCanvas.frames[0][2][row], current_values)

                    select_button = tk.Button(self.edit_window, text='Update', command=update_exercise)
                    select_button.grid(row=0, column=130, padx=0, sticky="w")

                    text_box = tk.scrolledtext.ScrolledText(self.edit_window, wrap=tk.WORD, height=5,
                                               font=("Times New Roman", 15))
                    text_box.insert(tk.INSERT, parent_row_values['instructions'])
                    text_box.grid(row=1, column=0, columnspan=132)

                    # Contains the String with the rows image location.
                    img_location_entry_var = tk.StringVar()
                    img_location_entry_var.set(edit_row.image)
                    img_location_entry = tk.Entry(self.edit_window, textvariable=img_location_entry_var)
                    img_location_entry.grid(row=3, column=0, columnspan=129, sticky='WE')

                    def img_select_command():
                        print('img_select button clicked.')
                        f = fd.askopenfilename(defaultextension=".jpg")
                        # TODO: build out command code
                        log.log(logging.DEBUG, 'selected file: ' + str(f))

                        # Set the variable to the path of the selected image if an image is selected.
                        if f is not (None or ''):
                            img_location_entry_var.set(f)
                        else:
                            log.log(logging.DEBUG, 'Image selection cancelled')

                    img_select = tk.Button(self.edit_window, text='Browse', command=img_select_command)
                    img_select.grid(row=3, column=130, sticky='E')

                    # Closing edit_window
                    def on_closing_edit():
                        print('Edit window closed')
                        # TODO: update changed parameters.
                        text = text_box.get("1.0", tkinter.END)

                        self.edit_window.destroy()
                    self.edit_window.protocol("WM_DELETE_WINDOW", on_closing_edit)

                self.edit_button = tk.Button(frame, text="edit", command=edit_command)
                self.edit_button.grid(row=self.row, column=120, padx=0, sticky="w")

            def update_tag(self, title_label_tag, value_label_tag_value):
                self.title_label_tag.set(title_label_tag)
                self.value_label_tag_value.set(value_label_tag_value)

            def delete_row(self):
                """
                Removes a selected row from the canvas.
                Currently using the edit button.
                TODO: this does not currently remove the row entry from ScrolledCanvas.rows[].  May result in a memory
                leak if not addressed.
                :param index: The rows ID corresponding to the ListView position.  Stored in the ScrolledCanvasRow.
                :return:
                """
                self.select_button.grid_remove()
                self.label_tag.grid_remove()
                self.repetitions_label.grid_remove()
                self.repetitions_entry.grid_remove()
                self.hold_label.grid_remove()
                self.hold_entry.grid_remove()
                self.hold_units.grid_remove()
                self.sets_label.grid_remove()
                self.sets_entry.grid_remove()
                self.sets_label2.grid_remove()
                self.freq_num_label.grid_remove()
                self.freq_num_entry.grid_remove()
                self.freq_int_label.grid_remove()
                self.freq_int_option.grid_remove()
                self.edit_button.grid_remove()
                log.log(logging.DEBUG, 'row ' + str(index) + ' removed.')

            def restore_row(self):
                """
                Restores a selected row from the canvas.
                Currently using the edit button.
                :param index: The rows ID corresponding to the ListView position.  Stored in the ScrolledCanvasRow.
                :return:
                """
                self.select_button.grid()
                self.label_tag.grid()
                self.repetitions_label.grid()
                self.repetitions_entry.grid()
                self.hold_label.grid()
                self.hold_entry.grid()
                self.hold_units.grid()
                self.sets_label.grid()
                self.sets_entry.grid()
                self.sets_label2.grid()
                self.freq_num_label.grid()
                self.freq_num_entry.grid()
                self.freq_int_label.grid()
                self.freq_int_option.grid()
                self.edit_button.grid()
                log.log(logging.DEBUG, 'row ' + str(index) + ' restored.')

            def retrieve_current_values(self, target_row):
                """
                Retrieves the currently entered values of the specified row structure and returns them as a dictionary.
                :param target_row:
                :return: Dictionary.
                """
                # Copy initial values in the event some have not been updated.
                updated_dict = self.parameters_dict.copy()
                updated_dict['name'] = target_row.title_label_tag.get()
                updated_dict['repetitions'] = target_row.repetitions_entry.get()
                updated_dict['hold duration'] = target_row.hold_entry.get()
                updated_dict['hold units'] = target_row.hold_units.get()
                updated_dict['sets'] = target_row.sets_entry.get()
                updated_dict['times per period'] = target_row.freq_num_entry.get()
                updated_dict['period duration'] = target_row.freq_int_option.get()
                updated_dict['instructions'] = target_row.instructions
                updated_dict['image'] = target_row.image

                return updated_dict

            def update_row_values(self, target_row, set_dict):
                """
                Updates the values of the specified row with the values provided in dictionary set_dict.
                :param target_row: Target ScrolledCanvasRow object
                :param set_dict: Dictionary containing values to insert into row.  Follows parameters_dict.
                :return:
                """
                updated_dict = self.parameters_dict.copy()
                updated_dict = {**updated_dict, **set_dict}

                # update the original entries read from disk in data.exercise
                for index, entry in enumerate(data.exercise):
                    if entry['id'] == updated_dict['id']:
                        data.exercise[index] = {**data.exercise[index], **updated_dict}
                        # print('data.exercise[index]', data.exercise[index])

                # Copy back dictionary changes.  TODO: check for errors.
                self.parameters_dict = updated_dict

                target_row.title_label_tag.set(set_dict['name'])
                target_row.repetitions_entry_var.set(set_dict['repetitions'])
                target_row.hold_entry_var.set(set_dict['hold duration'])
                target_row.hold_units.set(set_dict['hold units'])
                target_row.sets_entry_var.set(set_dict['sets'])
                target_row.freq_num_entry_var.set(set_dict['times per period'])
                target_row.freq_int_option.set(set_dict['period duration'])
                target_row.instructions = set_dict['instructions']
                target_row.image = set_dict['image']

                print('updated_dict: ', updated_dict)
                return updated_dict

        def insert_value(frame_index, row, left, right):
            """
            Insert values into ScrolledCanvas
            :param row:
            :param left:
            :param right:
            :return:
            """
            self.title_label_tag = tk.StringVar()
            self.value_label_tag_value = tk.StringVar()

            self.title_label_tag.set(left)
            self.value_label_tag_value.set(right)

            # insert labels into frame4. Left side.
            self.label_tag = tk.Label(details_list.frames[frame_index][0], textvariable=self.title_label_tag,
                                      justify=tk.LEFT, wraplength=100)
            self.label_tag.grid(row=row, column=0, padx=5, sticky="w")

            # insert labels into frame4. Right side.
            self.label_tag_value = tk.Label(details_list.frames[frame_index][0],
                                            textvariable=self.value_label_tag_value, justify=tk.LEFT, wraplength=160)
            print("self.label_tag_value = ", self.value_label_tag_value.get())
            self.label_tag_value.grid(row=row, column=1, padx=5, sticky="w")

        # insert all values for a specific device into the ScrolledCanvas
        def insert_user_values(user):
            for index, row in enumerate(details_list.rows, start=0):
                row.update_tag('', '')
            details_list.rows = []
            details_list.canvas_position = 0

        data = Sample_Data.Data
        # Insert sample data into exercise list scrolled frame
        for index, entry in enumerate(data.exercise):
            print(details_list.frames[0]) # TODO
            details_list.frames[0][2].append(ScrolledCanvasRow(details_list.frames[0][0], index, entry))

        # Insert exercise groups into exercise group section.
        # Horizontally across upper-top area.
        exercise_groups = ExerciseGroups(exercise_group_sel, data.exercise_sets)

        def hide_frame_rows(frame):
            """
            Hide frame rows
            :param frame: Integer index of list containing frames
            :return:
            """
            for row in details_list.frames[frame][2]:
                row.delete_row()

        # Insert second frame for testing.
        # details_list.frames.append([None, None, []])
        details_list.append_frames_entry()
        details_list.frames[1][0] = (tk.Frame(details_list.page_4_canvas, bg='red'))
        details_list.frames[1][1] = (details_list.page_4_canvas.create_window((5, 5), window=details_list.frames[1][0],
                                                                              anchor='nw'))

        """
        for index, entry in enumerate(data.exercise):
            print(details_list.frames[1])
            details_list.frames[1][2].append(ScrolledCanvasRow(details_list.frames[1][0], index, entry))
        """

        # hide frame 1
        hide_frame_rows(1)

        # Exercise Type Control Button Frame
        sel_type_comm_frame = tk.Frame(ctr_mid, bg='grey95', width=200)
        sel_type_comm_frame.grid(row=1, column=0, sticky="nsew", columnspan=1, rowspan=1)

        # Resize contents of sel_type_comm_frame
        sel_type_comm_frame.grid_columnconfigure(0, weight=0)
        sel_type_comm_frame.grid_columnconfigure(1, weight=3)
        #sel_type_comm_frame.grid_columnconfigure(2, weight=2)
        sel_type_comm_frame.grid_rowconfigure(0, weight=0)
        sel_type_comm_frame.grid_rowconfigure(10, weight=2)

        # Exercise Type Group Label
        grp_lbl = tk.Label(ctr_mid, text='Exercise Types', borderwidth=2, relief="ridge")
        grp_lbl.grid(row=0, column=0, columnspan=2, sticky='ew')

        # Scrolled Canvas Exercise Selector
        # To the left of the main ScrolledCanvas.
        scroll_canvas_sel = ListboxObjects(sel_type_comm_frame, 1)
        scroll_canvas_sel.frame.grid(row=0, column=1, sticky="nsew", columnspan=1, rowspan=30, padx=(4,0))


        def type_btn_default_cmd():
            #details_list.frames[0][2][0].restore_default_values()
            ScrolledCanvasRow.restore_default_values()
        type_btn_default = tk.Button(sel_type_comm_frame, text='Default', width=6, command=type_btn_default_cmd)
        type_btn_default.grid(row=0, column=0)

        def type_btn_select_cmd():
            scroll_canvas_sel.group_action_exercises()
        type_btn_select = tk.Button(sel_type_comm_frame, text='Select', width=6, command=type_btn_select_cmd)
        type_btn_select.grid(row=1, column=0)

        type_btn_new = tk.Button(sel_type_comm_frame, text='New', width=6,
                                 command=lambda: prompt_new(scroll_canvas_sel, 'groups'))
        type_btn_new.grid(row=2, column=0)

        type_btn_edit = tk.Button(sel_type_comm_frame, text='Edit', width=6)
        type_btn_edit.grid(row=3, column=0)

        type_btn_delete = tk.Button(sel_type_comm_frame, text='Delete', width=6)
        type_btn_delete.grid(row=4, column=0)

        # Insert and manage exercise groups in the Scrolled Canvas Selector.
        # Insert the rest of the dynamically selected exercise groups.
        scroll_canv_exercises = ExerciseGroups(scroll_canvas_sel, data.exercise_types, start=1)

        # insert 'all' exercise group to contain all exercises.
        all_ex_grp = [{'All': []}]
        for index, exercise in enumerate(data.exercise):
            all_ex_grp[0]['All'].append({'id': exercise['id']})
        log.log(logging.DEBUG,
                'all_ex_grp: : ' + str(all_ex_grp))

        # Update data.exercise_types with an entry for all of the exercises available.
        data.exercise_types.append(all_ex_grp[0])
        # log.log(logging.DEBUG, 'data.exercise_types after all_ex_grp: : ' + str(data.exercise_types))

        scroll_canv_exercises.insert_exercise_groups(all_ex_grp, start=0)

        # log.log(logging.DEBUG, 'exercise_groups: self.exercise_group_layout: ' +
        # str(scroll_canv_exercises.exercise_group_layout))

        """
        # Insert the names of the exercise types into the scroll_canvas_sel listbox.
        for index, exercise_type in enumerate(data.exercise_types):
            for exercise in exercise_type:
                scroll_canvas_sel.listbox.insert(index, str(exercise))
        """



        #details_list.frames[0].grid_forget()

        #details_list.page_4_canvas.tag_raise(details_list.frames[1][1])
        #details_list.page_4_canvas.tag_lower(details_list.frames[0][1])
        #for index, entry in enumerate(data.exercise, start=8):
        #    details_list.rows.append(ScrolledCanvasRow(details_list.frames[1], index, entry))

        # Testing code to insert dummy rows into frame
        #for row in range(0, 20):
        #    insert_value(1, row, row, 33)

        #details_list.page_4_canvas.tag_raise(details_list.frames_id[0])
        #details_list.page_4_canvas.tag_raise(details_list.frames_id[0])

        # Brings the selected frame to the front.

        # Test code.  Hides all rows from frame[0]
        #for row in details_list.frames[0][2]:
        #    row.delete_row()
        # details_list.frames[0][2][0].delete_row()
        # details_list.frames[0][2][2].delete_row()
        # details_list.frames[0][2][3].delete_row()
        # details_list.rows[2].restore_row()
        #details_list.frames[0][0].lift(aboveThis=None)  # bring frame to top of ScrolledCanvas
        #details_list.frames[1].lift(aboveThis=None)


if __name__ == "__main__":
    root = tk.Tk()
    # MainApplication(root).pack(side="top", fill="both", expand=True)
    m_app = MainApplication(root).grid()
    root.mainloop()

