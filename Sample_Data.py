
class Data:
    # Sample exercise data to load into the exercise selector.
    exercise = [
        {
            'id': 1,
            'name': 'Exercise 1',
            'repetitions': '6',
            'hold duration': '7',
            'hold units': 'minutes',
            'sets': '8',
            'times per period': '9',
            'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 2,
            'name': 'Exercise 2',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 3,
            'name': 'Exercise 3',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 4,
            'name': 'Exercise 4',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 5,
            'name': 'Exercise 5',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 6,
            'name': 'Exercise 6',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 7,
            'name': 'Exercise 7',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 8,
            'name': 'Exercise 8',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 9,
            'name': 'Exercise 9',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 10,
            'name': 'Exercise 10',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 11,
            'name': 'Exercise 11',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 12,
            'name': 'Exercise 12',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 13,
            'name': 'Exercise 13',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        },
        {
            'id': 14,
            'name': 'Exercise 14',
            'repetitions': '5',
            'hold duration': '5',
            'hold units': 'seconds',
            'sets': '5',
            'times per period': '2',
            'period duration': 'day',
            'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                            'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                            ' "Bridge" with your body. Hold and then lower yourself and repeat.',
            'image': 'images/001.jpg',
            'layout': 1
        }
    ]

    exercise_sets = [
        {'set 1': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 2': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 3': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 4': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 5': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 6': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 7': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        {'set 8': [
            {
                'id': 1,
                'name': 'Exercise 1',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 2,
                'name': 'Exercise 2',
                'repetitions': '4',
                'hold duration': '3',
                'hold units': 'minutes',
                'sets': '2',
                'times per period': '1',
                'period duration': 'hour',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
            {
                'id': 6,
                'name': 'Exercise 6',
                'repetitions': '5',
                'hold duration': '5',
                'hold units': 'seconds',
                'sets': '5',
                'times per period': '2',
                'period duration': 'day',
                'instructions': 'While lying on your back with knees bent, tighten your lower abdominals, '
                                'squeeze your buttocks and then raise your buttocks off the floor/bed as creating a'
                                ' "Bridge" with your body. Hold and then lower yourself and repeat.',
                'image': 'images/001.jpg',
                'layout': 1
            },
        ]},
        #{'set 2': [1, 4, 5]},
        #{'set 3': [2, 3, 6]},
        #{'set 4': [2, 3, 6]},
    ]

    exercise_types = [
        {'Cervical': [
            {
                'id': 1,
            },
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            ]},
        {'Thoracic': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Lumbar': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Shoulder': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Elbow': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Wrist': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Knee': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Ankle': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Band - Upper Extremity': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]},
        {'Band - Knee': [
            {
                'id': 2,
            },
            {
                'id': 3,
            },
            {
                'id': 4,
            },
            {
                'id': 5,
            },
            {
                'id': 6,
            },
        ]}
        ]